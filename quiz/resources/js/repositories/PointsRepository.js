import Repository from 'axios';

const resource = '/points';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    getForUser(userId) {
        return Repository.get(`user${userId}${resource}`);
    },

}
