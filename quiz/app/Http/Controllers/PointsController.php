<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PointsController extends Controller
{
    public $successStatus = 200;

    public function addPoints(Request $request)
    {
        $user = $this->modifyUser($request->all());

        $success = $user.update();

        return response()->json(['success' => $success], $this->successStatus);
    }


    protected function modifyUser(array $data)
    {
        return User([
            'scored_points' => $data['name'],
        ]);
    }

}
